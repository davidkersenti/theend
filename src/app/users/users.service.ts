import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2'; 

@Injectable()
export class UsersService {

   // private _url = "http://jsonplaceholder.typicode.com/users";

    usersObservable;
   

    getUsers(){
    this.usersObservable = this.af.database.list('/users').map(
      users =>{
        users.map(
          user => {
            user.posTitles = [];
            for(var p in user.posts){
                user.posTitles.push(
                this.af.database.object('/posts/' + p)
              )
            }
          }
        );
        return users;
      }
    )
    //this.usersObservable = this.af.database.list('/users');
    return this.usersObservable;
	}
  addUser(user){
    this.usersObservable.push(user);
  }

  updateUser(user){
  let userKey = user.$key;
  let userData = {name:user.name, email:user.email};
  this.af.database.object('/users/' + userKey).update(userData);


}
deleteUser(user){

  let userKey = user.$key;
  this.af.database.object('/users/' + userKey).remove();
}

 constructor(private af:AngularFire) { }

}
