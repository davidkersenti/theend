import { Component, OnInit } from '@angular/core';
import {ProductsService} from './products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styles: [`
    .products li { cursor: default; }
    .products li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class ProductsComponent implements OnInit {

  constructor(private _productsService: ProductsService) {
    //this.users = this._userService.getUsers();
  }

  products;
  currentProduct;
  isLoading = true;

   select(product){
		this.currentProduct = product; 
    console.log(	this.currentProduct);
 }
 deleteProduct(product){
  this._productsService.deleteProduct(product);
   }

  ngOnInit() {
        this._productsService.getProducts()
			    .subscribe(products => {this.products = products;
                               this.isLoading = false;
                               console.log(products)});
  }

}