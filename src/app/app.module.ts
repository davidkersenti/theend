import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import{AngularFireModule} from 'angularfire2';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RouterModule, Routes } from '@angular/router';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts/posts.service';
import { PostComponent } from './post/post.component';
import { PostFormComponent } from './post-form/post-form.component';
import { ProductsComponent } from './products/products.component';
import { ProductsService } from './products/products.service';
import { ProductComponent } from './product/product.component';
import { CategoryComponent } from './category/category.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoicesService } from './invoices/invoices.service';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoiceComponent } from './invoice/invoice.component';


export const firebaseConfig = {
   apiKey: "AIzaSyAybWqWd2Dbr6A6TPi3aZF-t3l5kfSAPoo",
    authDomain: "users-352f2.firebaseapp.com",
    databaseURL: "https://users-352f2.firebaseio.com",
    projectId: "users-352f2",
    storageBucket: "users-352f2.appspot.com",
    messagingSenderId: "914352984198"
}

const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'invoices', component: InvoicesComponent },
  { path: 'invoiceForm', component: InvoiceFormComponent }, 
  { path: '', component: InvoiceFormComponent },
  { path: '**', component: PageNotFoundComponent }
];




@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    UserFormComponent,
    PageNotFoundComponent,
    PostsComponent,
    PostComponent,
    PostFormComponent,
    ProductsComponent,
    ProductComponent,
    CategoryComponent,
    InvoicesComponent,
    InvoiceFormComponent,
    InvoiceComponent,
    
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
     RouterModule,
    RouterModule.forRoot(appRoutes),
    RouterModule.forRoot(appRoutes),
   AngularFireModule.initializeApp(firebaseConfig)
   
  ],
  providers: [UsersService,PostsService,ProductsService,InvoicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
